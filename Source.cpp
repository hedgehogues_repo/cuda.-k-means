#pragma once

#include <string>
#include <iostream>
#include <time.h>
#include "headers.h"

using namespace std;

int Check(string etalon, string checked)
{
	unsigned char rChecked, rEtalon;
	unsigned char gChecked, gEtalon;
	unsigned char bChecked, bEtalon;
	unsigned char aChecked, aEtalon;
	long int n;
	long int m;
	FILE *inChecked;
	FILE *inEtalon;

	inEtalon = fopen(etalon.c_str(), "rb");
	inChecked = fopen(checked.c_str(), "rb");

	fread(&n, sizeof(int), 1, inChecked);
	fread(&m, sizeof(int), 1, inChecked);
	fread(&n, sizeof(int), 1, inEtalon);
	fread(&m, sizeof(int), 1, inEtalon);

	for (long int i = 0; i < n * m; ++i)
	{
		fread(&rEtalon, sizeof(unsigned char), 1, inEtalon);
		fread(&gEtalon, sizeof(unsigned char), 1, inEtalon);
		fread(&bEtalon, sizeof(unsigned char), 1, inEtalon);
		fread(&aEtalon, sizeof(unsigned char), 1, inEtalon);

		fread(&rChecked, sizeof(unsigned char), 1, inChecked);
		fread(&gChecked, sizeof(unsigned char), 1, inChecked);
		fread(&bChecked, sizeof(unsigned char), 1, inChecked);
		fread(&aChecked, sizeof(unsigned char), 1, inChecked);
		if (rChecked != rEtalon)
		{
			return 0;
		}
	}
	fclose(inChecked);
	fclose(inEtalon);
	return 1;
}

int main1()
{
	string inFile;
	vector <pair<int, int> > vec;
	srand(time(NULL) );
	int n = 0;
	int c1 = 0;
	int c2 = 0;
	int c3 = 0;
	//for (int i = 0; i < 10000; ++i)
	{
		//cout << i << endl;
		n = 10;
		vec.push_back(make_pair(202, 40) );
		vec.push_back(make_pair(213, 35) );
		vec.push_back(make_pair(167, 282));
		vec.push_back(make_pair(102, 295));
		vec.push_back(make_pair(75, 290));
		vec.push_back(make_pair(39, 369));
		vec.push_back(make_pair(120, 150));
		vec.push_back(make_pair(108, 233));
		vec.push_back(make_pair(305, 349));
		vec.push_back(make_pair(324, 293));

		// cout << "Generated" << endl;
		// main_urv(-1, true, -1, -1, "", "1.data", 5, 6, -1, vec);
		cout << "Urvanov_GPU" << endl;
		main_urv(0, true, 8, 512, "22.data", "2U_G.data", -1, -1, n, vec);
		cout << "Urvanov_CPU" << endl;
		main_urv(0, false, -1, -1, "22.data", "2U_C.data", -1, -1, n, vec);
		cout << "Vinogradov" << endl;
		main_vinog("22.data", "2V.data", n, vec);
		cout << "UrvanovCPU and UrvanovGPU:" << endl;
		c1 = Check("2U_C.data", "2U_G.data");
		c2 = Check("2U_C.data", "2V.data");
		c3 = Check("2U_G.data", "2V.data");
		cout << c1 << endl;
		cout << "UrvanovCPU and Vinogradov:" << endl;
		cout << c2 << endl;
		cout << "UrvanovGPU and Vinogradov:" << endl;
		cout << c3 << endl;
		if (c1 == 0 || c2 == 0 || c3 == 0)
		{
			//break;
		}
		cout << endl;
	}
	return 0;
}