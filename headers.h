#pragma once
#include <string>
#include <vector>

using namespace std;

int main_urv(int type, bool isGPU, int blocks, int threads, string inFile, string outFile, int width, int height, int n, vector <pair<int, int> > vec);
int main_vinog(string path_in, string path_out, int nc, vector <pair<int, int> > vec);