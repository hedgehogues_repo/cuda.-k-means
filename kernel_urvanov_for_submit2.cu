#include <stdio.h>
#include <math.h>
#include <fstream>
#include <iostream>
#include <string>

#include "cuda_runtime.h"
#include "device_launch_parameters.h"


using namespace std;

#define CUDA_ERROR(call) {                                        \
  cudaError err = call;                                           \
  if(err != cudaSuccess) {                                        \
    fprintf(stderr, "CUDA error in file '%s' in line %i: %s.\n",  \
    __FILE__, __LINE__, cudaGetErrorString(err));                 \
    exit(1);                                                      \
    }                                                             \
} while (0)

#define N_MAX_CENTROIDS 32

__constant__ double dev_centroidsX1[N_MAX_CENTROIDS];
__constant__ double dev_centroidsY1[N_MAX_CENTROIDS];
__constant__ double dev_centroidsZ1[N_MAX_CENTROIDS];


/*DEVICE*/

// �������, �������� ���������� ����� �������
__device__ double getDist2_GPU(double x, double y, double z, double centroidX, double centroidY, double centroidZ)
{
	double dx = x - centroidX;
	double dy = y - centroidY;
	double dz = z - centroidZ;
	return dx * dx + dy * dy + dz * dz;
}

// ����������� �������������� ����� ��������� ��������
__global__ void Dist_GPU
(
	unsigned char *x, unsigned char *y, unsigned char *z, 
	unsigned char *classVec,
	int n, int nCentroids
)
{
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	int delta = blockDim.x * gridDim.x;
	double distance2 = 0;
	double minDistance2 = 0;
	unsigned char minDistance2Index = 0;
	for (int i = tid; i < n; i += delta)
	{
		minDistance2 = 200000; // < 255 ^ 2 * 3
		minDistance2Index = 255;
		for (unsigned char k = 0; k < nCentroids; ++k)
		{
			distance2 = getDist2_GPU(x[i], y[i], z[i], dev_centroidsX1[k], dev_centroidsY1[k], dev_centroidsZ1[k]);
			if (distance2 < minDistance2)
			{
				minDistance2Index = k;
				minDistance2 = distance2;
			}
		}
		classVec[i] = minDistance2Index;
	}
	return;
}

// ������� ��������� ��� �������� �� ����� ��������
__global__ void FreeNewCentroids_GPU(double3 *newCentroids, unsigned int *counts)
{
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	newCentroids[tid].x = 0;
	newCentroids[tid].y = 0;
	newCentroids[tid].z = 0; 
	counts[tid] = 0;
	return;
}

// ���������� ������� ���������
__global__ void GetSumNewCentroids_GPU
(
	unsigned char *x, unsigned char *y, unsigned char *z,
	unsigned char *classVec,
	double3 *newCentroids,
	int n,
	unsigned int *counts
)
{
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	int delta = blockDim.x * gridDim.x;
	int cacheIndex = threadIdx.x;
	__shared__ double3 local_newCentroids[N_MAX_CENTROIDS];
	__shared__ unsigned int local_counts[N_MAX_CENTROIDS];

	for (int i = tid; i < n; i += delta)
	{
		int curIndex = classVec[i];
		local_newCentroids[curIndex].x += x[i];
		local_newCentroids[curIndex].y += y[i];
		local_newCentroids[curIndex].z += z[i];
		local_counts[curIndex] += 1;
	}

	__syncthreads();

	int i = blockDim.x / 2;

	while (i != 0)
	{
		if (cacheIndex < i)
		{
			local_newCentroids[cacheIndex].x += local_newCentroids[cacheIndex + i].x;
			local_newCentroids[cacheIndex].y += local_newCentroids[cacheIndex + i].y;
			local_newCentroids[cacheIndex].z += local_newCentroids[cacheIndex + i].z;
			local_counts[cacheIndex] += 1;
		}
		__syncthreads();
		i /= 2;
	}

	newCentroids[0] = local_newCentroids[0];
	counts[0] = local_counts[0];

	return;
}

/*HOST*/



// ��������� ������


void GenData(string outFile, int width, int height)
{
	int n = 0;
	FILE *out;

	out = fopen(outFile.c_str(), "wb");

	n = width * height;
	fwrite(&width, sizeof(int), 1, out);
	fwrite(&height, sizeof(int), 1, out);


	for (int i = 0; i < n; i++)
	{
		unsigned char r = (rand() * 1.0 / RAND_MAX) * 255;
		unsigned char g = (rand() * 1.0 / RAND_MAX) * 255;
		unsigned char b = (rand() * 1.0 / RAND_MAX) * 255;
		unsigned char a = 0;
		fwrite(&r, sizeof(unsigned char), 1, out);
		fwrite(&g, sizeof(unsigned char), 1, out);
		fwrite(&b, sizeof(unsigned char), 1, out);
		fwrite(&a, sizeof(unsigned char), 1, out);
	}

	fclose(out);
	return;
}

void TeachersData(string outFile)
{
	FILE *out;
	int width = 3;
	int height = 3;
	int a[3][3] =
	{
		{ 0x004CDFA2, 0x00FEC9F7, 0x0045D89E },
		{ 0x0053E8B4, 0x004DD199, 0x0056DD92 },
		{ 0x004CE0A9, 0x00FAD1F7, 0x00E9D0D4 },
	};

	out = fopen(outFile.c_str(), "wb");

	fwrite(&width, sizeof(int), 1, out);
	fwrite(&height, sizeof(int), 1, out);
	for (int i = 0; i < 3; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			fwrite(&a[i][j], sizeof(int), 1, out);
		}
	}
	fclose(out);
	return;
}





// ������ � ������

void ReadImageUrv
(
	int &nCentroids,
	double **centroidsX, double **centroidsY, double **centroidsZ,
	int &width, int &height,
	unsigned char **x, unsigned char **y, unsigned char **z,
	unsigned char **classVec, string inFile
)
{
	int n = 0;
	FILE *in = fopen(inFile.c_str(), "rb");
	fread(&width, sizeof(int), 1, in);
	fread(&height, sizeof(int), 1, in);
	n = width * height;
	*x = (unsigned char *)malloc(sizeof(unsigned char) * n);
	*y = (unsigned char *)malloc(sizeof(unsigned char) * n);
	*z = (unsigned char *)malloc(sizeof(unsigned char) * n);

	cin >> nCentroids;

	*centroidsX = (double *)malloc(sizeof(double) * nCentroids);
	*centroidsY = (double *)malloc(sizeof(double) * nCentroids);
	*centroidsZ = (double *)malloc(sizeof(double) * nCentroids);
	*classVec = (unsigned char *)malloc(sizeof(unsigned char) * n);

	for (int i = 0; i < n; i++)
	{
		fread(&(*x)[i], sizeof(unsigned char), 1, in);
		fread(&(*y)[i], sizeof(unsigned char), 1, in);
		fread(&(*z)[i], sizeof(unsigned char), 1, in);
		fread(&(*classVec)[i], sizeof(unsigned char), 1, in);
	}

	for (int i = 0; i < nCentroids; ++i)
	{
		int xCoord = 0;
		int yCoord = 0;
		cin >> xCoord >> yCoord;
		(*centroidsX)[i] = (double)(*x)[yCoord * width + xCoord];
		(*centroidsY)[i] = (double)(*y)[yCoord * width + xCoord];
		(*centroidsZ)[i] = (double)(*z)[yCoord * width + xCoord];
	}
	fclose(in);
	return;
}

void WriteImageUrv(int width, int height, unsigned char *x, unsigned char *y, unsigned char *z, unsigned char *classVec, string &outFile)
{
	int n = 0;
	FILE *out;

	out = fopen(outFile.c_str(), "wb");

	n = width * height;
	fwrite(&width, sizeof(int), 1, out);
	fwrite(&height, sizeof(int), 1, out);


	for (int i = 0; i < n; i++)
	{
		fwrite(&x[i], sizeof(unsigned char), 1, out);
		fwrite(&y[i], sizeof(unsigned char), 1, out);
		fwrite(&z[i], sizeof(unsigned char), 1, out);
		fwrite(&classVec[i], sizeof(unsigned char), 1, out);
	}

	fclose(out);
	return;
}


// ��������� � ������������ ������

void AllocateAndCopyDataToDevice
(
	unsigned char **dev_x, unsigned char **dev_y, unsigned char **dev_z, unsigned char **dev_classVec, unsigned int **dev_counts, double3 **dev_newCentroids,
	unsigned char *x, unsigned char *y, unsigned char *z, unsigned char *classVec, double *centroidsX, double *centroidsY, double *centroidsZ,
	int n, int nCentroids
)
{
	cudaMalloc(dev_x, sizeof(unsigned char) * n);
	cudaMalloc(dev_y, sizeof(unsigned char) * n);
	cudaMalloc(dev_z, sizeof(unsigned char) * n);
	cudaMalloc(dev_classVec, sizeof(unsigned char) * n);
	cudaMalloc(dev_counts, sizeof(unsigned int) * n);
	cudaMalloc(dev_newCentroids, sizeof(double3) * n);

	cudaMemcpy(*dev_x, x, sizeof(unsigned char) * n, cudaMemcpyHostToDevice);
	cudaMemcpy(*dev_y, y, sizeof(unsigned char) * n, cudaMemcpyHostToDevice);
	cudaMemcpy(*dev_z, z, sizeof(unsigned char) * n, cudaMemcpyHostToDevice);
	cudaMemcpy(*dev_classVec, classVec, sizeof(unsigned char) * n, cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(dev_centroidsX1, centroidsX, sizeof(double) * N_MAX_CENTROIDS);
	cudaMemcpyToSymbol(dev_centroidsY1, centroidsY, sizeof(double) * N_MAX_CENTROIDS);
	cudaMemcpyToSymbol(dev_centroidsZ1, centroidsZ, sizeof(double) * N_MAX_CENTROIDS);

	return;
}

void FreeData_GPU
(
	unsigned char *dev_x, unsigned char *dev_y, unsigned char *dev_z, unsigned int *dev_counts, double3 *dev_newCentroids,
	unsigned char *dev_classVec
)
{
	cudaFree(dev_x);
	cudaFree(dev_y);
	cudaFree(dev_z);
	cudaFree(dev_classVec);
	cudaFree(dev_counts);
	cudaFree(dev_newCentroids);
	return;
}

void FreeData_CPU
(
	unsigned char *x, unsigned char *y, unsigned char *z,
	double *centroidsX, double *centroidsY, double *centroidsZ,
	unsigned char *classVec
)
{
	free(x);
	free(y);
	free(z);
	free(classVec);
	free(centroidsX);
	free(centroidsY);
	free(centroidsZ);
	return;
}


// ������� ��� �������� ���������� ������ k-means

// �������, �������� ����������
__host__ double getDist2(double x, double y, double z, double centroidX, double centroidY, double centroidZ)
{
	double dx = x - centroidX;
	double dy = y - centroidY;
	double dz = z - centroidZ;
	return dx * dx + dy * dy + dz * dz;
}


// ���������� ���������� �� ������� ��� ���� �����
__host__ void Dist
(
	unsigned char *x, unsigned char *y, unsigned char *z,
	unsigned char *classVec,
	double *centroidsX, double *centroidsY, double *centroidsZ,
	int n, int nCentroids
)
{
	int tid = 0;
	int delta = 1;
	double distance2 = 0;
	double minDistance2 = 0;
	int minDistance2Index = -1;
	for (int i = tid; i < n; i += delta)
	{
		minDistance2 = 200000; // < 255 ^ 2 * 3
		minDistance2Index = -1;
		for (int k = 0; k < nCentroids; ++k)
		{
			distance2 = getDist2(x[i], y[i], z[i], centroidsX[k], centroidsY[k], centroidsZ[k]);
			if (distance2 < minDistance2)
			{
				minDistance2Index = k;
				minDistance2 = distance2;
			}
		}
		classVec[i] = minDistance2Index;
	}
	return;
}

// ������� ������ � ���������� ������� ��� ����� ��������
__host__ void FreeNewCentroids
(
	double3 *newCentroids,
	int nCentroids,
	unsigned int *counts
)
{
	int tid = 0;
	int delta = 1;

	for (int i = tid; i < nCentroids; i += delta)
	{
		newCentroids[i].x = 0;
		newCentroids[i].y = 0;
		newCentroids[i].z = 0;
		counts[i] = 0;
	}
}


// ���������� ������� ���������
__host__ void GetSumNewCentroids
(
	unsigned char *x, unsigned char *y, unsigned char *z,
	unsigned char *classVec,
	double3 *newCentroids,
	int n,
	unsigned int *counts
)
{
	int tid = 0;
	int delta = 1;

	for (int i = tid; i < n; i += delta)
	{
		int curIndex = classVec[i];
		newCentroids[curIndex].x += x[i];
		newCentroids[curIndex].y += y[i];
		newCentroids[curIndex].z += z[i];
		counts[curIndex] += 1;
	}
}

// ���������� ������� ���������
__host__ void GetNewCentroids(double *centroidsX, double *centroidsY, double *centroidsZ, double3 *newCentroids, int nCentroids, unsigned int *counts)
{
	int curIndex = 0;
	for (int tid = 0; tid < nCentroids; ++tid)
	{
		curIndex = tid;
		newCentroids[curIndex].x = newCentroids[curIndex].x / counts[curIndex];
		newCentroids[curIndex].y = newCentroids[curIndex].y / counts[curIndex];
		newCentroids[curIndex].z = newCentroids[curIndex].z / counts[curIndex];

		swap(newCentroids[tid].x, centroidsX[tid]);
		swap(newCentroids[tid].y, centroidsY[tid]);
		swap(newCentroids[tid].z, centroidsZ[tid]);
	}
	return;
}

// ������ ����������� ������� ���������
__host__ void MovingCentroids(double *centroidsX, double *centroidsY, double *centroidsZ, double3 *newCentroids, int nCentroids, bool &finish)
{
	for (int i = 0; i < nCentroids; i++)
	{
		if (getDist2(newCentroids[i].x, newCentroids[i].y, newCentroids[i].z, centroidsX[i], centroidsY[i], centroidsZ[i]) > 1e-6)
		{
			finish = false;
			break;
		}
	}
	return;
}


// ������� ������������
template<class TType>void Log(int n, TType *data, string logOut)
{
	FILE *out = fopen(logOut.c_str(), "wb");

	fwrite(&n, sizeof(int), 1, out);

	for (int i = 0; i < n; i++)
	{
		fwrite(&data[i], sizeof(TType), 1, out);
	}

	fclose(out);
}

void K_means
(
	unsigned char *x, unsigned char *y, unsigned char *z,
	unsigned char *classVec,
	double *centroidsX, double *centroidsY, double *centroidsZ,
	int n, int nCentroids,
	bool isGPU,
	int blocks,
	int threads
)
{
	/*Device*/
	unsigned char *dev_x = NULL;
	unsigned char *dev_y = NULL;
	unsigned char *dev_z = NULL;
	unsigned char *dev_classVec = NULL;
	unsigned int *dev_counts = NULL;
	double3 *dev_newCentroids = NULL;

	/*Host*/
	double3 *newCentroids = NULL;
	unsigned int *counts = NULL;
	bool finish = false;

	newCentroids = (double3 *) malloc(sizeof(double3) * nCentroids);
	counts = (unsigned int *) malloc(sizeof(unsigned int) * nCentroids);


	// ����� ������� ��������� ������ �� GPU � ����������� �� GPU
	AllocateAndCopyDataToDevice
	(
		&dev_x, &dev_y, &dev_z, &dev_classVec, &dev_counts, &dev_newCentroids,
		x, y, z, classVec, centroidsX, centroidsY, centroidsZ,
		n, nCentroids
	);
	while (true)
	{
		if (isGPU)
		{
			// ��������� ���������
			FreeNewCentroids(newCentroids, nCentroids, counts);

			// ������� ���������� � �������� � ���������
			Dist_GPU << <blocks, threads >> >(dev_x, dev_y, dev_z, dev_classVec, n, nCentroids);
			cudaMemcpy(classVec, dev_classVec, sizeof(unsigned char) * n, cudaMemcpyDeviceToHost);

			// ��������� ������ ��������� (��������� ��� ������ �������)
			GetSumNewCentroids(x, y, z, classVec, newCentroids, n, counts);
			GetNewCentroids(centroidsX, centroidsY, centroidsZ, newCentroids, nCentroids, counts);
			cudaMemcpyToSymbol(dev_centroidsX1, centroidsX, sizeof(double) * N_MAX_CENTROIDS);
			cudaMemcpyToSymbol(dev_centroidsY1, centroidsY, sizeof(double) * N_MAX_CENTROIDS);
			cudaMemcpyToSymbol(dev_centroidsZ1, centroidsZ, sizeof(double) * N_MAX_CENTROIDS);

			// ����������� ��������� ���������
			finish = true;
			MovingCentroids(centroidsX, centroidsY, centroidsZ, newCentroids, nCentroids, finish);
		}
		else
		{
			// ��������� ���������
			FreeNewCentroids(newCentroids, nCentroids, counts);

			// ������� ���������� � �������� � ���������
			Dist(x, y, z, classVec, centroidsX, centroidsY, centroidsZ, n, nCentroids);

			// ��������� ������ ��������� (��������� ��� ������ �������)
			GetSumNewCentroids(x, y, z, classVec, newCentroids, n, counts);
			GetNewCentroids(centroidsX, centroidsY, centroidsZ, newCentroids, nCentroids, counts);

			// ����������� ��������� ���������
			finish = true;
			MovingCentroids(centroidsX, centroidsY, centroidsZ, newCentroids, nCentroids, finish);
		}

		if (finish)
		{
			break;
		}
	}

	// ����� ������� ������� ������ �� GPU
	FreeData_GPU
	(
		dev_x, dev_y, dev_z,
		dev_counts, dev_newCentroids,
		dev_classVec
	);
	free(newCentroids);
	free(counts);
}

int main()
{
	/*Host*/
	// ������
	unsigned char *x = NULL;
	unsigned char *y = NULL;
	unsigned char *z = NULL;
	// ������ ������
	unsigned char *classVec = NULL;
	double *centroidsX = NULL;
	double *centroidsY = NULL;
	double *centroidsZ = NULL;
	// ������ ������
	int width = 0;
	int height = 0;
	int nCentroids = 0;
	int blocks = 8;
	int threads = 512;
	// �������/�������� �����
	string inFile = "";
	string outFile = "";
	cudaEvent_t start, stop;
	float t = 0;

	cin >> inFile >> outFile;

	ReadImageUrv(nCentroids, &centroidsX, &centroidsY, &centroidsZ, width, height, &x, &y, &z, &classVec, inFile);

	// ������������ ������������������ (������ ������� �������)
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	cudaEventRecord(start, 0);


	// ��������� k-means
	K_means(x, y, z, classVec, centroidsX, centroidsY, centroidsZ, width * height, nCentroids, true, blocks, threads);

	// ����� ������� �������
	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&t, start, stop);
	cudaEventDestroy(start);
	cudaEventDestroy(stop);
	cout << t << endl;

	// ����� ������
	WriteImageUrv(width, height, x, y, z, classVec, outFile);

	// ������� ������
	FreeData_CPU
	(
		x, y, z,
		centroidsX, centroidsY, centroidsZ,
		classVec
	);

    return 0;
}